#include<stdio.h>

typedef struct list {
	int num;
	struct list *next, *pre;
} list;
typedef struct def{
	int pos;
	char sign;
	list *head;
	list *tail;
} def;
typedef def* Number;
void initNumber(Number *ptr);
void storenum(Number *a1, char *str);
int NodesInNumber(Number number);
Number addnumbers(Number num1, Number num2);
Number subtractnumbers(Number num1, Number num2);
Number multipliaction(Number num1, Number num2);
Number division(Number num1, Number num2);
void destroy(Number *num);
void processNumber(Number *num);
void printNumber(Number a);
int isDigit(char ch);
